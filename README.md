# What?

# npm scripts

- `npm run download:katas` copies all katas that exist and provided in the https://github.com/tddbin/katas repo 
  an "copies" them in the `_katas-cache` directory, it copies all the files just as provided on https://katas.tddbin.com/
  (it uses the built branch `gh-pages` for that and just git-clones it)
- `npm run convert:kataToMarkdown <kata.js> <title> <kata.md>` convert a test file (a kata) to a markdown file, the `<title>` will
  become the H1 of the page 

