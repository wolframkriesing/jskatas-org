#!/usr/bin/env zx

// this used to be in the npm script:
//    mkdir -p _katas-cache; cd _katas-cache; git clone --depth 1 --branch gh-pages https://github.com/tddbin/katas.git .; cd -

import {$, cd, fs, chalk} from 'zx';

const KATAS_DIR_NAME = '_katas-cache';

if (fs.pathExistsSync(KATAS_DIR_NAME)) {
  console.log(chalk.red(`Directory "${KATAS_DIR_NAME}" does already exist. Maybe remove it first?\nStopping the script.`));
} else {
  await fs.mkdirp(KATAS_DIR_NAME);
  cd(KATAS_DIR_NAME);
  await $`git clone --depth 1 --branch gh-pages https://github.com/tddbin/katas.git .`;
  cd('..');
}
