import playwright from "playwright";

async function runTest(browserType) {
  const browser = await playwright[browserType].launch();
  const page = await browser.newPage();

  await page.goto('http://localhost:5004/tests/runner.html');
  // just wait a bit, so the browser can render all the stuff, this can definitely be improved.
  await new Promise((resolve) => setTimeout(resolve, 1000));
  const results = [];
  for (const li of await page.locator('success').all())
    results.push(await li.textContent());
  
  console.log(`${browserType.padEnd(10, ' ')} ${results.length} results:`, results.join(''));

  await browser.close();
}

await runTest('chromium');
await runTest('firefox');
await runTest('webkit');
