import {Kata, RawKataFromMetadata} from "./Kata";

type RawKataBundleFromMetadata = {
  name: string;
  nameSlug: Slug;
  items: RawKataFromMetadata[];
};

type KataBundleId = string;
type KataBundleConfig = {
  bundleId: KataBundleId;
  rootUrl?: string;
}

type KataBundleSiteData = {
  blogPostsForKataGroups?: KataGroupToBlogPostsMap;
}

export class KataBundle {
  id: KataBundleId;
  name: string;
  nameSlug: Slug;
  katas: Kata[];
  kataGroups: KataGroup[];
  rootUrl: string;
  blogPostsForKataGroups: KataGroupToBlogPostsMap;
  static fromRawMetadataObject: (obj: RawKataBundleFromMetadata, config: KataBundleConfig, siteData?: KataBundleSiteData) => KataBundle;
}

type ForTestingOnly = {
  fixtures: {
    oneRawKataBundleObject: (partial?: Partial<RawKataBundleFromMetadata>) => RawKataBundleFromMetadata;
  }
};
export const forTestingOnly: ForTestingOnly;

// A KataGroup is an array of katas, with an optional property `blogPosts`.
// So this is a pure array, with some sugar (the extra property) this might feel confusing but hey we are in JS land.
interface KataGroup extends Array<Kata> {
  blogPosts?: KataGroupToBlogPostsMap;
}
