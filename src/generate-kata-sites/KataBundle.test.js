import {describe, it} from '../test.js';
import {
  assertThat,
  hasProperties,
  throws,
  instanceOf,
  Matcher,
  containsString,
  contains,
  hasProperty
} from 'hamjest';
import {forTestingOnly, KataBundle} from './KataBundle.js';
import {forTestingOnly as forTestingKataOnly, Kata} from './Kata.js';

/**
 * @typedef {import('./KataBundle').RawKataBundleFromMetadata} RawKataBundleFromMetadata
 * @typedef {import('./Kata').RawKataFromMetadata} RawKataFromMetadata
 */

const { oneRawKataBundleObject } = forTestingOnly.fixtures;
const { oneRawKataObject } = forTestingKataOnly.fixtures;

/**
 * @param partial {Partial<Kata>}
 * @return {import('hamjest').Matcher}
 */
const isKataWithProps = (partial) => new Matcher({
  /**
   * @param maybeKata {any}
   * @return {true}
   * @throws
   */
  matches: function(maybeKata) {
    assertThat(maybeKata, instanceOf(Kata));
    assertThat(maybeKata, hasProperties(partial));
    return true; // the above throw otherwise
  },
  /**
   * @param description {import('hamjest').Description}
   */
  describeTo: function(description) {
    // irrelevant, since the matchers in `matches()` throw with the according message, on failure
  },
});

/**
 * @param partial {Partial<KataBundle>}
 * @return {import('hamjest').Matcher}
 */
const isKataBundleWithProps = (partial) => new Matcher({
  /**
   * @param maybeKata {any}
   * @return {true}
   * @throws
   */
  matches: function(maybeKata) {
    assertThat(maybeKata, instanceOf(KataBundle));
    assertThat(maybeKata, hasProperties(partial));
    return true; // the above throw otherwise
  },
  /**
   * @param description {import('hamjest').Description}
   */
  describeTo: function(description) {
    // irrelevant, since the matchers in `matches()` throw with the according message, on failure
  },
});

/** @type {function(RawKataBundleFromMetadata): KataBundle} */
const oneBundle = raw => KataBundle.fromRawMetadataObject(raw, {bundleId: ''});

describe('The KataBundle object', () => {
  describe('initialized with a metadata object (`KataBundle.fromRawMetadataObject()`)', () => {
    it('WHEN an empty object is given THEN throw', () => {
      // @ts-ignore
      assertThat(() => { oneBundle({}); },
        throws(hasProperties({message: 'Missing props in KataBundle object: name, nameSlug, items'}))
      );
    });
    it('WHEN name+nameSlug given THEN provide them', () => {
      const bundle = oneBundle({ name: 'ES10', nameSlug: 'es10', items: [] });
      assertThat(bundle, isKataBundleWithProps({ name: 'ES10', nameSlug: 'es10', katas: [] }))
    });
    it('WHEN props have the wrong type THEN throw and tell so', () => {
      let errorMessage;
      const notAString = 0;
      const notAnArray = '';
      const wrongTypedRawKata = {
        name: notAString,
        nameSlug: notAString,
        items: notAnArray,
      }
      try {
        // @ts-ignore
        oneBundle(wrongTypedRawKata);
      } catch (error) {
        errorMessage = /** @type {Error} */(error).message;
      }
      assertThat(errorMessage, containsString('name (string)'));
      assertThat(errorMessage, containsString('nameSlug (string)'));
      assertThat(errorMessage, containsString('items (array)'));
    });
  });

  describe('verify the Katas given (provided in `items`)', () => {
    it('WHEN one kata given THEN provide a `Kata` instance AND name and description', () => {
      const kataItem = oneRawKataObject({name: 'kata', description: ''});
      const bundle = oneBundle(oneRawKataBundleObject({ items: [kataItem] }));
      assertThat(bundle.katas[0], isKataWithProps({ name: 'kata', description: '' }))
    });
  });

  describe('get the katas by group', () => {
    it('WHEN each kata has its own group THEN there are as many groups as katas', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata', description: '', groupName: 'one'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'two'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'three'}),
      ];
      const bundle = oneBundle(oneRawKataBundleObject({ items: kataItems }));
      assertThat(bundle.katas.length, bundle.kataGroups.length);
    });
    it('WHEN all three katas are in the same group THEN there is just one kataGroup', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata', description: '', groupName: 'one'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'one'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'one'}),
      ];
      const bundle = oneBundle(oneRawKataBundleObject({ items: kataItems }));
      assertThat(bundle.kataGroups, hasProperties({length: 1}));
    });
    it('WHEN groups have multiple katas THEN the groups are sorted by most katas first', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata', description: '', groupName: 'one'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'two'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'two'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'three'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'three'}),
        oneRawKataObject({name: 'kata', description: '', groupName: 'three'}),
      ];
      const bundle = oneBundle(oneRawKataBundleObject({ items: kataItems }));
      assertThat(bundle.kataGroups, contains(
        hasProperties({ length: 3 }),
        hasProperties({ length: 2 }),
        hasProperties({ length: 1 }),
      ));
    });
  });
  
  describe('GIVEN there are additional `siteData`', () => {
    it('WHEN there is a blog posts for the kata group THEN it is provided as `blogPosts`', () => {
      const kataItems = [
        oneRawKataObject({name: 'kata', description: '', groupName: 'The Group name'}),
      ];
      const blogPost = {title: 'Blog post title', url: 'url/to/blogpost'};
      const blogPostsForKataGroups = new Map([['The Group name', [blogPost]]]);
      const bundle = KataBundle.fromRawMetadataObject(
        oneRawKataBundleObject({ items: kataItems }), 
        {bundleId: ''}, 
        {blogPostsForKataGroups}
      );
      assertThat(bundle.kataGroups[0], hasProperty('blogPosts', [{title: 'Blog post title',url: 'url/to/blogpost'}]));
    });
  });
});
