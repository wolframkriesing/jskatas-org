export class KataMarkdownFile {
  /**
   * @param {Path} kataRoot
   * @param {Filename} filename
   * @param {string} markdownText
   * @return {import('./KataMarkdownFile').KataMarkdownFile}
   */
  static fromFile(kataRoot, filename, markdownText) {
    const sourceFile = /** @type {import('./KataMarkdownFile').KataMarkdownFile} */(new KataMarkdownFile());
    sourceFile.filename = filename;
    sourceFile.kataRoot = kataRoot;
    sourceFile.markdownText = markdownText;
    return sourceFile;
  }
}
