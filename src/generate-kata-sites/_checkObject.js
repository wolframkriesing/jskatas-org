import typeOf from 'typeof';

/**
 * @param var1 {any}
 * @param var2 {any}
 * @return {boolean}
 */
const areSameType = (var1, var2) => {
  return typeOf(var1) === typeOf(var2);
}

/**
 * @param obj {PlainObject}
 * @param _requiredProps {PlainObject}
 * @return {{ missing: string[], wrongTypes: string[], isValid: boolean }}
 */
export const checkObject = (obj, _requiredProps) => {
  const o = _requiredProps;
  const requiredProps = Object.keys(o);
  const missing = requiredProps.filter(prop => Reflect.has(obj, prop) === false);
  const wrongTypes = requiredProps
    .filter(prop => areSameType(obj[prop], Reflect.get(o, prop)) === false)
    .map(name => `${name} (${typeOf(Reflect.get(o, name))})`)
  ;
  const isValid = missing.length === 0 && wrongTypes.length === 0;
  return { missing, wrongTypes, isValid };
};
