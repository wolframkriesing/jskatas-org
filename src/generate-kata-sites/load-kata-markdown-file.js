// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {findKataSourceFilenames, readFile} from '../_deps/fs.js';
import {KataMarkdownFile} from './KataMarkdownFile.js';

const prodDeps = () => {
  return {findKataSourceFilenames, readFile};
};

export const loadManyKataMarkdownFiles = ({findKataSourceFilenames, readFile} = prodDeps()) => async (katasPath) => {
  const files = await findKataSourceFilenames(katasPath);
  return await Promise.all(files.map(async (filename) => {
    return KataMarkdownFile.fromFile(katasPath, filename, await readFile(filename));
  }));
};
