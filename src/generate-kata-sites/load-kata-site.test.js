// @ts-nocheck TODO make this type checked, but for getting 0 errors for now, some files dont get checked yet. Improve it!
import {strict as assert} from 'assert';
import hamjest from 'hamjest';
const {assertThat, hasProperties, instanceOf, everyItem} = hamjest;

import {loadKataSite, loadManyKataSites} from './load-kata-site.js';
import {describe, it} from '../test.js';
import {KataSite} from './KataSite.js';
import {KataMarkdownFile} from './KataMarkdownFile.js';

describe('Load a kata-site, with all data ready to render', () => {
  describe('GIVEN one kata-site source file', () => {
    const loadPost = async (params) => {
      const defaults = {markdownText: '', markdownFilename: 'es1/global/parseInt.md'};
      const {markdownText, markdownFilename} = {...defaults, ...params};
      const sourceFile = KataMarkdownFile.fromFile('', markdownFilename, markdownText);
      return await loadKataSite()(sourceFile);
    };
    it('WHEN site ONLY has a headline and a first paragraph THEN provide: url, slug, urlForKataGroup, dateCreated, markdownFilename, headline and abstract', async () => {
      const post = await loadPost({
        markdownFilename: 'es1/global/parseInt.md',
        markdownText: 'dateCreated: 2021-12-26T13:56:42.025Z  \n\n# parseInt()\nsummary in first paragraph'
      });
      const expectedProps = {
        url: '/katas/es1/global/parseInt/',
        slug: 'parseInt',
        urlForKataGroup: '/katas/es1/',
        dateCreated: '2021-12-26T13:56:42.025Z',
        headline: 'parseInt()',
        headlineAsHtml: 'parseInt()',
        abstract: 'summary in first paragraph',
        abstractAsHtml: 'summary in first paragraph',
      };
      assertThat(post, instanceOf(KataSite));
      assertThat(post, hasProperties(expectedProps));
    });

    // the content parsing ...
    it('WHEN it has no first paragraph THEN set abstract=""', async () => {
      const post = await loadPost({markdownText: '# headline'});
      assert.equal(post.abstract, '');
      assert.equal(post.abstractAsHtml, '');
    });
    it('WHEN the headline is not followed by a paragraph, but e.g. another headline THEN set abstract=""', async () => {
      const post = await loadPost({markdownText: '# headline\n## subheadline'});
      assert.equal(post.abstractAsHtml, '');
    });
    it('WHEN it has NO metadata THEN the data set via metadata are either empty or have the right type', async () => {
      const post = await loadPost({markdownText: '# headline only'});
      assertThat(post, hasProperties({tags: [], oldUrls: []}));
    });
    it('WHEN it has only invalid metadata THEN the data set via metadata are either empty or have the right type', async () => {
      const post = await loadPost({markdownText: 'invalidMetadata: psst\n\n# headline only'});
      assertThat(post, hasProperties({
        tags: [],
        oldUrls: [],
        youtubeId: '',
        vimeoId: '',
        videoStartTime: '',
      }));
    });
    describe('WHEN it has metadata', async () => {
      it('AND headline and an abstract THEN find the headline and the abstract', async () => {
        const post = await loadPost({markdownText: 'meta: data\n\n# headline\nabstract, yeah'});
        assertThat(post, hasProperties({headline: 'headline', abstractAsHtml: 'abstract, yeah'}));
      });
      it('WHEN it has the metadata `dateCreated` THEN provide the property accordingly', async () => {
        const dateCreated = '2001-01-01 01:01 CET';
        const post = await loadPost({markdownText: `dateCreated: ${dateCreated}\n\n# headline\nabstract, yeah`});
        assertThat(post, hasProperties({dateCreated}));
      });
      it('WHEN it has the metadata `tags` THEN provide the property accordingly', async () => {
        const post = await loadPost({markdownText: `tags: tag1, tag2\n\n# headline\nabstract, yeah`});
        assertThat(post, hasProperties({tags: [{value: 'tag1', slug: 'tag1'}, {value: 'tag2', slug: 'tag2'}]}));
      });
      it('WHEN it has `oldUrls` THEN provide them', async () => {
        const post = await loadPost({markdownText: 'oldUrls: /blog/old.html /blog/old1.html\n\n# headline'});
        assertThat(post, hasProperties({oldUrls: ['/blog/old.html', '/blog/old1.html']}));
      });
      it('WHEN it has NO `oldUrls` (but other metadata) THEN provide an empty array for `oldUrls`', async () => {
        const post = await loadPost({markdownText: 'tags: none\n\n# headline'});
        assertThat(post, hasProperties({oldUrls: []}));
      });
      it('WHEN it has `youtubeId` THEN provide it', async () => {
        const post = await loadPost({markdownText: 'youtubeId: 12345\n\n# headline'});
        assertThat(post, hasProperties({youtubeId: '12345', hasVideo: true}));
      });
      it('WHEN it has `vimeoId` THEN provide it', async () => {
        const post = await loadPost({markdownText: 'vimeoId: 12345\n\n# headline'});
        assertThat(post, hasProperties({vimeoId: '12345', hasVideo: true}));
      });
      it('WHEN it has `videoStartTime` THEN provide it', async () => {
        const post = await loadPost({markdownText: 'videoStartTime: 42\n\n# headline'});
        assertThat(post, hasProperties({videoStartTime: '42', hasVideo: false}));
      });
      it('WHEN it has `isDraft: true` THEN provide isDraft=true', async () => {
        const post = await loadPost({markdownText: 'isDraft: true  \n\n# headline'});
        assertThat(post, hasProperties({isDraft: true}));
      });
      it('WHEN it has `isDraft: something else but true` THEN provide isDraft=false', async () => {
        const post = await loadPost({markdownText: 'isDraft: something else but true  \n\n# headline'});
        assertThat(post, hasProperties({isDraft: false}));
      });
      it('WHEN it has `isDraft: false` THEN provide isDraft=false', async () => {
        const post = await loadPost({markdownText: 'isDraft: false  \n\n# headline'});
        assertThat(post, hasProperties({isDraft: false}));
      });
      it('WHEN it does NOT have `isDraft` THEN provide isDraft=false', async () => {
        const post = await loadPost({markdownText: '# headline'});
        assertThat(post, hasProperties({isDraft: false}));
      });
      it('WHEN it has `previewImage` THEN provide `previewImageUrl`', async () => {
        const post = await loadPost({
          markdownFilename: '2020/12/21-post.md',
          markdownText: 'previewImage: road.jpg  \n\n# headline',
        });
        assertThat(post, hasProperties({previewImageUrl: '/katas/2020/12/21-post/road.jpg',}));
      });
      it('WHEN it has `canonicalUrl` THEN provide it', async () => {
        const post = await loadPost({
          markdownText: 'canonicalUrl: http://some.other.site/1  \n\n# H1',
        });
        assertThat(post, hasProperties({canonicalUrl: 'http://some.other.site/1',}));
      });
      it('WHEN it has `canonicalHint` THEN provide it', async () => {
        const post = await loadPost({
          markdownText: 'canonicalHint: This post is from somewhere  \n\n# H1',
        });
        assertThat(post, hasProperties({canonicalHint: 'This post is from somewhere',}));
      });
    });
    it('THEN provide `bodyAsHtml` without metadata, headline and first paragraph, etc.', async () => {
      const markdownText = 'tags: none\ndateCreated: 2000-01-01 10:00\n\n# headline\nfirst paragraph\n\nafter first paragraph';
      const post = await loadPost({markdownText});
      assertThat(post, hasProperties({bodyAsHtml: '<p>after first paragraph</p>\n'}));
    });
    it('THEN provide `abstractAsHtml` for previewing posts', async () => {
      const markdownText = [
        '# headline',
        '1st paragraph with [a link][1]',
        '',
        '2nd paragraph should not be rendered',
        '',
        '[1]: http://picostitch.com'
      ].join('\n');
      const post = await loadPost({markdownText});
      const expected = '1st paragraph with <a href="http://picostitch.com">a link</a>';
      assertThat(post, hasProperties({abstractAsHtml: expected}));
    });
    it('WHEN the headline contains markup THEN render the headlineAsHtml converted to HTML', async () => {
      const markdownText = [
        '# CSS `rem` **and** `em`',
        'irrelevant paragraph',
      ].join('\n');
      const post = await loadPost({markdownText});
      const expectedHtmlHeadline = 'CSS <code>rem</code> <strong>and</strong> <code>em</code>';
      assertThat(post, hasProperties({headlineAsHtml: expectedHtmlHeadline}));
    });
  });
  it('GIVEN many kata markdown files THEN load all the KataSite items', async () => {
    const markdownText = '# headline\nabstract';
    const manySourceFiles = [
      KataMarkdownFile.fromFile('', 'es1/array/sort.md', markdownText),
      KataMarkdownFile.fromFile('', 'es6/number/isinteger.md', markdownText),
    ];
    const posts = await loadManyKataSites()(manySourceFiles);
    assert.equal(posts.length, 2);
    const expectedAttributes = {headline: 'headline', abstractAsHtml: 'abstract'};
    assertThat(posts, everyItem(instanceOf(KataSite)));
    assertThat(posts[0], hasProperties({...expectedAttributes}));
    assertThat(posts[1], hasProperties({...expectedAttributes}));
  });
});
