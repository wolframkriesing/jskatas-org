type TestFileToMarkdownDeps = {
  now: Date;
}
type TestFileToMarkdownParams = {
  fileName: Filename;
  title: string;
  subtitle: string;
}

export function testFileToMarkdown(deps?: TestFileToMarkdownDeps):
  (params: TestFileToMarkdownParams) => Promise<string>;
