import {inspectFile, stats} from 'tests-inspector';

/**
 * @typedef {import('tests-inspector').Test} Test
 * @typedef {import('tests-inspector').Suite} Suite
 */

/**
 * @param tests {Test[]}
 * @return {string[]}
 */
const testsToList = (tests) => {
  return tests.map(test => `* ${test.name}`);
};

/**
 * @param suite {Suite}
 * @param headingLevel {number}
 * @return {string[]}
 */
const suiteToParagraph = (suite, headingLevel) => {
  const headingPrefix = Array(headingLevel).fill('#').join('');
  return [
    `${headingPrefix} ${suite.name}\n`,
    ...testsToList(suite.tests),
    ``,
    ...suite.suites.flatMap(suite => suiteToParagraph(suite, headingLevel + 1)),
  ];
};

/**
 * @param {import("./load-kata").TestFileToMarkdownDeps} deps
 * @return {function({fileName: Filename, title: string, subtitle: string}): Promise<string>}
 */
export const testFileToMarkdown = ({ now} = { now: new Date() }) => async ({fileName, title, subtitle}) => {
  const tests = await inspectFile(fileName);
  const lines = [
    `dateCreated: ${new Date(now).toISOString()}  `,
    ``,
    `# ${title}\n`,
    `${subtitle}`,
    ``,
    ...tests.suites.flatMap(suite => suiteToParagraph(suite, 2)),
  ];
  return {
    tests,
    markdown: lines.join('\n') + '\n',
    stats: {numTests: stats(tests).counts.tests},
  };
};
