/*
const chronicle = {
  builtins:{
    'AggregateError': {},
    'Array': {},
    'ArrayBuffer': {},
    'AsyncFunction': {},
    'AsyncGenerator': {},
    'AsyncGeneratorFunction': {},
    'AsyncIterator': {},
    'Atomics': {},
    'BigInt': {},
    'BigInt64Array': {},
    'BigUint64Array': {},
    'Boolean': {},
    'DataView': {},
    'Date': {},
    'decodeURI()': {},
    'decodeURIComponent()': {},
    'encodeURI()': {},
    'encodeURIComponent()': {},
    'Error': {},
    'escape()Deprecated': {},
    'eval()': {},
    'EvalError': {},
    'FinalizationRegistry': {},
    'Float32Array': {},
    'Float64Array': {},
    'Function': {},
    'Generator': {},
    'GeneratorFunction': {},
    'globalThis': {},
    'Infinity': {},
    'Int16Array': {},
    'Int32Array': {},
    'Int8Array': {},
    'InternalErrorNon-standard': {},
    'Intl': {},
    'isFinite()': {},
    'isNaN()': {},
    'Iterator': {},
    'JSON': {},
    'Map': {},
    'Math': {},
    'NaN': {},
    'Number': {},
    'Object': {},
    'parseFloat()': {},
    'parseInt()': {},
    'Promise': {},
    'Proxy': {},
    'RangeError': {},
    'ReferenceError': {},
    'Reflect': {},
    'RegExp': {},
    'Set': {},
    'SharedArrayBuffer': {},
    'String': {},
    'Symbol': {},
    'SyntaxError': {},
    'TypedArray': {},
    'TypeError': {},
    'Uint16Array': {},
    'Uint32Array': {},
    'Uint8Array': {},
    'Uint8ClampedArray': {},
    'undefined': {},
    'unescape()Deprecated': {},
    'URIError': {},
    'WeakMap': {},
    'WeakRef': {},
    'WeakSet': {},
  },
  expressions: {
    'async function': {},
    'async function*': {},
    'class expression': {},
    'function expression': {},
    'function* expression': {},
  },
  operators: {
    Addition (+): {}},
    Addition assignment (+=): {}},
    Assignment (=): {},
    await: {},
    Bitwise AND (&): {},
    Bitwise AND assignment (&=): {},
    Bitwise NOT (~): {},
    Bitwise OR (|): {},
    Bitwise OR assignment (|=): {},
    Bitwise XOR (^): {},
    Bitwise XOR assignment (^=): {},
    Comma operator (,): {},
    Conditional (ternary) operator: {},
    Decrement (--): {},
    delete: {},
    Destructuring assignment: {},
    Division (/): {},
    Division assignment (/=): {},
    Equality (==): {},
    Exponentiation (**): {},
    Exponentiation assignment (**=): {},
    Greater than (>): {},
    Greater than or equal (>=): {},
    Grouping operator ( ): {},
    import.meta: {},
    import(): {},
    in: {},
    Increment (++): {},
    Inequality (!=): {},
    instanceof: {},
    Left shift (<<): {},
    Left shift assignment (<<=): {},
    Less than (<): {},
    Less than or equal (<=): {},
    Logical AND (&&): {},
    Logical AND assignment (&&=): {},
    Logical NOT (!): {},
    Logical OR (||): {},
    Logical OR assignment (||=): {},
    Multiplication (*): {},
    Multiplication assignment (*=): {},
    new: {},
    new.target: {},
    null: {},
    Nullish coalescing assignment (??=): {},
    Nullish coalescing operator (??): {},
    Object initializer: {},
    Operator precedence: {},
    Optional chaining (?.): {},
    Property accessors: {},
    Remainder (%): {},
    Remainder assignment (%=): {},
    Right shift (>>): {},
    Right shift assignment (>>=): {},
    Spread syntax (...): {},
    Strict equality (===): {},
    Strict inequality (!==): {},
    Subtraction (-): {},
    Subtraction assignment (-=): {},
    super: {},
    this: {},
    typeof: {},
    Unary negation (-): {},
    Unary plus (+): {},
    Unsigned right shift (>>>): {},
    Unsigned right shift assignment (>>>=): {},
    void operator: {},
    yield: {},
    yield*    
  },
  statements: {},
  declarations: {},
  functions: {},
  classes: {},
  // Regular expressions
  // Errors
  // Misc
}
*/

import * as es1 from "./jschronicle/es1.js";
import * as es2 from "./jschronicle/es2.js";
import * as es3 from "./jschronicle/es3.js";
import * as es5_1 from "./jschronicle/es5_1.js";
import * as es6 from "./jschronicle/es6-2015.js";
import * as es7 from "./jschronicle/es7-2016.js";
import * as es8 from "./jschronicle/es8-2017.js";
import * as es9 from "./jschronicle/es9-2018.js";
import * as es10 from "./jschronicle/es10-2019.js";
import * as es11 from "./jschronicle/es11-2020.js";
import * as es12 from "./jschronicle/es12-2021.js";
import * as es13 from "./jschronicle/es13-2022.js";
import * as es14 from "./jschronicle/es14-2023.js";

const chronicle = {
  ES1: es1.stats.builtins,
  ES2: es2.stats.builtins,
  ES3: es3.stats.builtins,
  ES5: es5_1.stats.builtins,
  ES6: es6.stats.builtins,
  ES2016: es7.stats.builtins,
  ES2017: es8.stats.builtins,
  ES2018: es9.stats.builtins,
  ES2019: es10.stats.builtins,
  ES2020: es11.stats.builtins,
  ES2021: es12.stats.builtins,
  ES2022: es13.stats.builtins,
  ES2023: es14.stats.builtins,
};

// take every key in obj and return the length of the deepest nested object and sum them all up
const inDepthLength = obj => {
  let sum = 0;
  Object.entries(obj).forEach(([key, value]) => {
    if (typeof value === 'object') {
      sum += inDepthLength(value);
    } else {
      sum ++;
    }
  });
  return sum;
};

// Object.entries(chronicle).forEach(([key, value]) => {
//   console.log(key, inDepthLength(value), Object.entries(value).map(([key, value]) => [key, inDepthLength(value)])); 
// });


const meta = {
  ES1: es1.meta,
  ES2: es2.meta,
  ES3: es3.meta,
  ES5: es5_1.meta,
  ES6: es6.meta,
  ES2016: es7.meta,
  ES2017: es8.meta,
  ES2018: es9.meta,
  ES2019: es10.meta,
  ES2020: es11.meta,
  ES2021: es12.meta,
  ES2022: es13.meta,
  ES2023: es14.meta,
};

const facts = {
  ES1: [
    'First edition of ECMAScript, standardized in 1997.',
    'Defined Array, String, Object, Function, Boolean, Number, Date, Math, RegExp, Error, and the global object.',
    'Introduced the concept of prototypes, already then.',
    'Already in this version, `eval` is a property of the global object 🤷🏻.',
  ],
  ES2: [
    'Second edition of ECMAScript, standardized in 1998.',
    'No significant changes to version 1. [Wikipedia](https://en.wikipedia.org/wiki/ECMAScript_version_history) says mainly "editorial changes to keep [...] aligned with ISO/IEC 16262".',
  ],
  ES3: [
    'Third edition of ECMAScript, standardized in 1999.',
    'Added try/catch, regular expressions, better string handling, new control statements, and more.',
    'Introduced the concept of strict mode.',
  ],
  ES4: [
    'Fourth edition of ECMAScript, standardized in 2008.',
  ],
  ES5: [
    'Fifth edition of ECMAScript, standardized in 2009.',
  ],
  ES6: [
    'Sixth edition of ECMAScript, standardized in 2015.',
    'Also known as ECMAScript 2015.',
    'Added let, const, arrow functions, classes, modules, promises, and more.',
  ],
  ES2016: [
    'Seventh edition of ECMAScript, standardized in 2016.',
    'Added the `includes` method to Array.',
  ],
  ES2017: [
    'Eighth edition of ECMAScript, standardized in 2017.',
    'Added `Object.values`, `Object.entries`, `Object.getOwnPropertyDescriptors`, `String.padStart`, `String.padEnd`, and more.',
  ],
  ES2018: [
    'Ninth edition of ECMAScript, standardized in 2018.',
    'Added `RegExp.dotAll`, `RegExp.match`, `RegExp.replace`, `RegExp.search`, `RegExp.split`, `Promise.prototype.finally`, `Object.spread`, and more.',
  ],
  ES2019: [
    'Tenth edition of ECMAScript, standardized in 2019.',
    'Added `Array.flat`, `Array.flatMap`, `Object.fromEntries`, `String.trimStart`, `String.trimEnd`, `Symbol.description`, and more.',
  ],
  ES2020: [
    'Eleventh edition of ECMAScript, standardized in 2020.',
    'Added `String.matchAll`, `BigInt.asIntN`, `BigInt.asUintN`, `Promise.allSettled`, and more.',
  ],
  ES2021: [
    'Twelfth edition of ECMAScript, standardized in 2021.',
    'Added `String.replaceAll`, `Promise.any`, and more.',
  ],
  ES2022: [
    'Thirteenth edition of ECMAScript, standardized in 2022.',
    'Added `Array.at`.',
  ],
  ES2023: [
    'Fourteenth edition of ECMAScript, standardized in 2023.',
  ],
}

export {chronicle, facts, meta};