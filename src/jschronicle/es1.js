// The methods 'toString' and 'valueOf' often have a special behavior defined in es1, 
// but they exist already as inherited method from Object, so it's not listed in any children of Object.

const meta = {
  name: 'es1',
  longName: 'ECMAScript 1',
  publishedAt: ['June', 1997],
  specUrl: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_1st_edition_june_1997.pdf',
};

const stats = {
  builtins: {
    global: {
      properties: ['Infinity', 'NaN'],
      methods: ['eval', 'parseInt', 'parseFloat', 'escape', 'unescape', 'isNaN', 'isFinite'],
      constructors: [
        'Array',
        'Boolean',
        'Date',
        'Function',
        'Number',
        'Object',
        'String',
        // Math is not here since it is not usable as a constructor
      ],
      functions: [
        'Array',
        'Boolean',
        'Date',
        'Function',
        'Number',
        'Object',
        'String',
        // Math is not here since it is not callable as a function
      ]
    },

    Array: {
      staticProperties: ['prototype', 'length'],
      instanceProperties: ['constructor', 'length'],
      instanceMethods: [
        'reverse', 'join', 'sort',
      ],
    },
    Boolean: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor'],
      instanceMethods: [],
    },
    Date: {
      staticProperties: ['prototype', 'parse', 'UTC'],
      staticMethods: [],
      instanceProperties: ['constructor', ],
      instanceMethods: [
        'getTime', 'getYear', 
        'getFullYear', 'getUTCFullYear', 
        'getMonth','getUTCMonth', 
        'getDate','getUTCDate', 
        'getDay', 'getUTCDay', 
        'getHours', 'getUTCHours', 
        'getMinutes', 'getUTCMinutes', 
        'getSeconds', 'getUTCSeconds', 
        'getMilliseconds', 'getUTCMilliseconds', 
        'getTimezoneOffset', 
        'setTime', 
        'setMilliseconds', 'setUTCMilliseconds',
        'setSeconds', 'setUTCSeconds',
        'setMinutes', 'setUTCMinutes',
        'setHours', 'setUTCHours',
        'setDate', 'setUTCDate',
        'setMonth', 'setUTCMonth',
        'setFullYear', 'setUTCFullYear',
        'setYear',
        'toUTCString', 'toGMTString', 'toLocaleString', 
        ],
    },
    Function: {
      staticProperties: ['prototype', 'length'],
      instanceProperties: ['constructor', 'length', 'prototype', 'arguments'],
      instanceMethods: [
      ],
    },
    Math: {
      staticProperties: ['E', 'LN10', 'LN2', 'LOG2E', 'LOG10E', 'PI',  'SQRT1_2', 'SQRT2'],
      staticMethods: ['abs', 'acos', 'asin', 'atan', 'atan2', 'ceil', 'cos', 'exp', 'floor', 'log', 'max', 'min', 'pow', 'random', 'round', 'sin', 'sqrt', 'tan'],
      instanceProperties: [],
      instanceMethods: [],
    },
    Number: {
      staticProperties: ['prototype', 'MAX_VALUE', 'MIN_VALUE', 'NaN', 'NEGATIVE_INFINITY', 'POSITIVE_INFINITY'],
      staticMethods: [],
      instanceProperties: ['constructor'],
      instanceMethods: [],
    },
    Object: {
      staticProperties: ['prototype'],
      instanceProperties: ['constructor'],
      instanceMethods: ['valueOf', 'toString'],
    },
    String: {
      staticProperties: ['prototype'],
      staticMethods: ['fromCharCode'],
      instanceProperties: ['constructor', 'length'],
      instanceMethods: [
        'charAt',
        'charCodeAt',
        'indexOf',
        'lastIndexOf',
        'split',
        'substring',
        'toLowerCase',
        'toUpperCase',
      ],
    },
  },
};

export {stats, meta};
