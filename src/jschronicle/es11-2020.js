const meta = {
  name: 'es2020, es11',
  longName: 'ECMAScript 11',
  publishedAt: ['June', 2020],
  specUrl: 'https://262.ecma-international.org/11.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [
        'globalThis',
      ],
      methods: [],
      constructors: [],
      functions: [
        'BigInt',
      ]
    },
    BigInt: {
      staticMethods: [
        'asIntN',
        'asUintN',
      ],
      staticProperties: [
        'prototype',
      ],
      instanceMethods: [
        'toLocaleString',
        'toString',
        'valueOf',
      ],
      instanceProperties: [
        'constructor',
      ],
    },
    Promise: {
      instanceMethods: [
        'allSettled',
      ],
    },
  },
};
export {stats, meta};
