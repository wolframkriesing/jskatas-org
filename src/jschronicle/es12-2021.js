const meta = {
  name: 'es2021, es12',
  longName: 'ECMAScript 12',
  publishedAt: ['June', 2021],
  specUrl: 'https://262.ecma-international.org/12.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [
        'WeakRef',
        'FinalizationRegistry',
      ],
      functions: []
    },
    String: {
      instanceMethods: [
        'replaceAll',
      ],
    },
    Promise: {
      staticMethods: [
        'any',
      ],
    },
    AggregateError: {
      instanceProperties: [
        'errors',
      ],
    },
    WeakRef: {
      staticProperties: [
        'prototype',
      ],
      instanceMethods: [
        'deref',
      ],
      instanceProperties: [
        'constructor',
      ],
    },
    FinalizationRegistry: {
      staticProperties: [
        'prototype',
      ],
      instanceMethods: [
        'register',
        'unregister',
      ],
      instanceProperties: [
        'constructor',
      ],
    }
  },
};
export {stats, meta};
