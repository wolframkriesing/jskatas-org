const meta = {
  name: 'es2022, es13',
  longName: 'ECMAScript 13',
  publishedAt: ['June', 2022],
  specUrl: 'https://262.ecma-international.org/13.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Array: {
      instanceMethods: [
        'at',
      ],
    },
    Object: {
      staticMethods: [
        'hasOwn',
      ],
    }
  },
};
export {stats, meta};
