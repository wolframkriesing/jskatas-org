const meta = {
  name: 'es2',
  longName: 'ECMAScript 2',
  publishedAt: ['August', 1998],
  specUrl: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_2nd_edition_august_1998.pdf',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },

    Function: {
      staticProperties: [],
      instanceProperties: [
        // 'arguments' seems to have been removed, it was still in ES1
      ],
      instanceMethods: [],
    },
  },
};

export {stats, meta};
