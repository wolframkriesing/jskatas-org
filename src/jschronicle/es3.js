// The methods 'toString' and 'valueOf' often have a special behavior defined in es1, 
// but they exist already as inherited method from Object, so it's not listed in any children of Object.

const meta = {
  name: 'es3',
  longName: 'ECMAScript 3',
  publishedAt: ['December', 1999],
  specUrl: 'https://www.ecma-international.org/wp-content/uploads/ECMA-262_3rd_edition_december_1999.pdf',
};

const stats = {
  builtins: {
    global: {
      properties: ['undefined'],
      methods: [
        // 'escape', 'unescape' have been removed, they are not in es3 anymore, but had been in es2
        'decodeURI', 'decodeURIComponent',
        'encodeURI', 'encodeURIComponent',
      ],
      constructors: [
        'RegExp',
        'Error',
        'EvalError',
        'RangeError',
        'ReferenceError',
        'SyntaxError',
        'TypeError',
        'URIError',
      ],
      functions: []
    },

    Array: {
      staticProperties: [],
      instanceProperties: [],
      instanceMethods: [
        'concat', 'pop', 'push', 'shift', 'slice', 'splice', 'unshift',
      ],
    },
    Boolean: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Date: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'toDateString', 'toTimeString', 'toLocaleDateString', 'toLocaleTimeString',

        // 'getYear' is not in the spec anymore (even though it also claims to not be part of the spec)
        // 'setYear', 'toGMTString' have also been removed
      ],
    },
    Error: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    EvalError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    Function: {
      staticProperties: [],
      instanceProperties: [],
      instanceMethods: [
        'apply', 'call',
      ],
    },
    Math: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [],
    },
    Number: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: ['toFixed', 'toExponential', 'toPrecision'],
      instanceMethods: [],
    },
    Object: {
      staticProperties: [],
      instanceProperties: [],
      instanceMethods: ['toLocaleString', 'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable'],
    },
    RangeError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    ReferenceError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    RegExp: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: [
        'constructor',
        'global', 'ignoreCase', 'lastIndex', 'multiline', 'source',
      ],
      instanceMethods: ['exec', 'test',],
    },
    String: {
      staticProperties: [],
      staticMethods: [],
      instanceProperties: [],
      instanceMethods: [
        'concat',
        'localeCompare',
        'match',
        'replace',
        'search',
        'slice',
        'toLocaleLowerCase', 'toLocaleUpperCase',
      ],
    },
    SyntaxError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    TypeError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },
    URIError: {
      staticProperties: ['prototype'],
      staticMethods: [],
      instanceProperties: ['constructor', 'name', 'message'],
      instanceMethods: [],
    },

  },
};

export {stats, meta};
