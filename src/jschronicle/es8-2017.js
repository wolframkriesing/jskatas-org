const meta = {
  name: 'es2017, es8',
  longName: 'ECMAScript 8',
  publishedAt: ['June', 2017],
  specUrl: 'https://262.ecma-international.org/8.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Object: {
      staticProperties: [],
      staticMethods: [
        'values',
        'entries',
        'getOwnPropertyDescriptors'
      ],
      instanceProperties: [],
      instanceMethods: [],
    },
  },
};
export {stats, meta};
