const meta = {
  name: 'es2018, es9',
  longName: 'ECMAScript 9',
  publishedAt: ['June', 2018],
  specUrl: 'https://262.ecma-international.org/9.0/',
};

const stats = {
  builtins: {
    global: {
      properties: [],
      methods: [],
      constructors: [],
      functions: []
    },
    Promise: {
      instanceMethods: [
        'finally',
      ],
    },
    RegExp: {
      instanceProperties: [
        'dotAll',
      ],
    },
  },
};
export {stats, meta};
