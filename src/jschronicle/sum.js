import * as es1 from "./es1.js";
import * as es2 from "./es2.js";
import * as es3 from "./es3.js";
import * as es5_1 from "./es5_1.js";
import * as es6 from "./es6-2015.js";
import * as es7 from "./es7-2016.js";
import * as es8 from "./es8-2017.js";

const mergeBuiltins = (builtins) => {
  const merged = {};
  for (const builtin of builtins) {
    for (const [type, data] of Object.entries(builtin)) {
      if (false === type in merged) {
        merged[type] = {};
      }
      for (const [key, values] of Object.entries(data)) {
        const existingValues = merged[type]?.[key] ?? [];
        merged[type][key] = [...new Set([...existingValues, ...values])].sort(); // merge and remove duplicates
      }
    }
  }
  return merged;
}

const mergeSpecs = (specs) => {
  const merged = {
    builtins: mergeBuiltins(specs.map(spec => spec.builtins)),
  };
  return merged;
};

const allProps = mergeSpecs([
  es1.stats, 
  es2.stats, 
  es3.stats, 
  es5_1.stats, 
  es6.stats,
  es7.stats,
  es8.stats,
  // es9.stats
]);

console.log(JSON.stringify(allProps, null, 4));