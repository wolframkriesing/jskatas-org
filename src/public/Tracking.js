/**
 * @typedef {{passed: boolean}} Test
 */

export class Tracking {
  
  _testEventsTracked = new Set();
  /**
   * @param deps {{track: function(string): void}
   */
  constructor(deps) {
    this.track = deps.track;
  }
  /**
   * @param {Test[]} tests
   */
  trackTestProgress(tests) {
    const numTests = tests.length;
    if (numTests === 0) return;
    
    const numPassedTests = tests.filter(test => test.passed).length;
    if (numPassedTests === 1) {
      if (false === this._testEventsTracked.has('1st Test Passed')) {
        this.track('1st Test Passed');
        this._testEventsTracked.add('1st Test Passed');
      }
    }
    
    if (Math.round(numTests/2) === numPassedTests) {
      if (false === this._testEventsTracked.has('50% Tests Passed')) {
        this.track('50% Tests Passed');
        this._testEventsTracked.add('50% Tests Passed');
      }
    }
    
    if (numTests === numPassedTests) {
      if (false === this._testEventsTracked.has('100% Tests Passed')) {
        this.track('100% Tests Passed');
        this._testEventsTracked.add('100% Tests Passed');
      }
    }    
  }
}
