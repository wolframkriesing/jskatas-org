const confettiTemplate = document.createElement('template');
const colors = [
  'brown', 'darkgoldenrod', 'rebeccapurple', 'palegreen', 'blue', 'palevioletred', 'burlywood', 'hotpink', 'aquamarine', 'firebrick', 'green', 'fuchsia'
];
const speedRange = {min: 1, max: 5}
const particleCount = 150;
const particleSizeRange = [1, 10];

const oneShape = (colorValue, dimension) => {
  const halfDimension = Math.round(dimension / 2);
  const shapes = [
    `<polygon points="0,5 ${halfDimension},0 ${dimension},${dimension}" style="fill:${colorValue}" />`,
    `<circle cx="2.5" cy="2.5" r="2.5" style="fill:${colorValue}" />`,
    `<rect x="0" y="0" width="${dimension}" height="${dimension}" style="fill:${colorValue}" />`
  ];
  return shapes[Math.floor(Math.random() * shapes.length)];
}

const oneParticle = () => {
  const speedValue = (Math.random() * (speedRange.max - speedRange.min) + speedRange.min).toFixed(1);
  const colorValue = colors[Math.floor(Math.random() * colors.length)];
  const dimension = (Math.random() * (particleSizeRange[1] - particleSizeRange[0]) + particleSizeRange[0]).toFixed(1);
  const spreadDistance = (Math.random() * 100 - 50).toFixed(1) + "vw"; // Random between -50vw and +50vw
  return `
    <svg height="5" width="5" style="--speed: ${speedValue}; --spread-distance: ${spreadDistance}">
      ${oneShape(colorValue, dimension)}
    </svg>`;
}

confettiTemplate.innerHTML = `
  ${Array(particleCount).fill(0).map(_ => oneParticle()).join('')}
  
  <style>
    :host {
      pointer-events: none;
      position: fixed;
      top: 0;
      left: 0;
      width: 100vw;
      height: 100vh;
      margin: 0 auto;
      padding: 0 10px;
      display: flex;
      justify-content: space-evenly;
    }

    @keyframes rising {
        0% {
          bottom: 0;
          left: 50%;
          opacity: 0;
          transform: rotate(0deg) translate(-50%, 0);
        }
        25% {
          opacity: 1;
        }
        100% {
          bottom: 100%;
          left: calc(50% + var(--spread-distance, 0px));
          opacity: 0.5;
          transform: rotate(720deg);
        }
      }
    
      svg {
        position: absolute;
        bottom: 0;
        left: 50%;
        transform: translate(-50%, 0);
        opacity: 0;
        animation-name: rising;
        animation-duration: calc(var(--speed) * 1s);
        animation-iteration-count: 2;
        animation-timing-function: ease-out;
      }
  </style>
`;

class Confetti extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({mode: 'open'});
  }

  connectedCallback() {
    this.shadowRoot.appendChild(confettiTemplate.content.cloneNode(true));
  }
}

customElements.define('jskatas-confetti', Confetti);
