const kataEditorTemplate = document.createElement('template');
kataEditorTemplate.innerHTML = `
  <style>
    .container {
      border: 1px solid transparent;
      border-radius: 4px;
      padding: var(--spacing-small);
    }
    .container.focus {
      background: var(--color-background-2nd);
      border-color: var(--color-foreground-2nd);
      box-shadow: 0 4px 8px var(--color-foreground-2nd);
    }
    .focus #editor {
      background: var(--color-background-2nd);
    }
    
    .container.focus.success {
      border-color: lightgreen;
    }
    .success #editor,
    .container.success {
      background: #EEFFEE;
    }
    .container.error {
      border-color: var(--color-foreground-highlight-2nd);
    }
    #editor {
      height: 4rem;
      line-height: 1.1rem;
      margin: 0;
      font-family: var(--font-family-main);
      font-size: var(--font-size-small);
    }
    
    .result {
      display: none;
      margin-bottom: 0;
      background: var(--color-background-main);
      padding: var(--spacing-small);
    }
    
    .reset {
      display: none;
      color: var(--color-foreground-main);
      padding: 0 var(--spacing-small);
      border: 1px outset var(--color-foreground-highlight);
      border-radius: var(--border-radius-smaller);
      background: var(--color-foreground-highlight-2nd);
    }
    
    .reset:hover {
      cursor: pointer;
      transition: background-color 0.5s;
      color: var(--color-foreground-inverse);
      background: var(--color-foreground-highlight);
      box-shadow: 0 0 1px 1px var(--color-foreground-highlight-3rd);
    }
    
    .result.errors {
      color: red;
      gap: var(--spacing-medium);
      align-items: baseline;
      justify-content: space-between;
    }
    
    .result.errors .message {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      width: 100%;
      min-width: 0;
      padding: 0 var(--spacing-small) 0;
      border: solid transparent;
      border-width: 1px 0;
    }
    
    .result.errors:hover {
      display: initial;
    }
    
    .result.errors:hover .reset {
      display: initial;
    }
    
    .result.success {
      color: green;
    }
    
    #editor .ace_scrollbar {
      scrollbar-width: none;
    }
  </style>
    
  <section class="container">
    <pre id="editor"></pre>
    <p class="result"></p>
  </section>
`;

class TestEditor extends HTMLElement {

  _isLoaded = false;

  /** @type {RunnerOptions} */
  #runnerOptions = {strictMode: true, topLevelAwait: false, runGivenCodeOnly: false};

  static get observedAttributes() {
    return ['autocompletion'];
  }

  constructor() {
    super();
    this.attachShadow({mode: 'open'});
  }

  _selectInShadowRoot(selector) {
    return this.shadowRoot && this.shadowRoot.querySelector(selector);
  }

  connectedCallback() {
    this.shadowRoot.appendChild(kataEditorTemplate.content.cloneNode(true));
    this.$container = this._selectInShadowRoot('.container');
    const $editor = this._selectInShadowRoot('#editor');

    const autoCompletion = this.getAttribute('autocompletion') === 'off' ? false : true;

    ace.require("ace/ext/language_tools");
    const editor = ace.edit($editor, {
      mode: "ace/mode/javascript",
      selectionStyle: "text",
      enableBasicAutocompletion: autoCompletion,
      enableLiveAutocompletion: autoCompletion,
      behavioursEnabled: autoCompletion,
      // Remove most of the UI stuff, so the pure editor is seen.
      showLineNumbers: false,
      showGutter: false,
      showPrintMargin: false,
    });
    editor.renderer.attachToShadowRoot();
    editor.setTheme("ace/theme/chrome");
    editor.session.setMode("ace/mode/javascript");

    editor.session.on('change', () => updateEditorHeight(editor));
    updateEditorHeight(editor);
    editor.session.on('change', () => this.runTest());
    editor.session.on('reset', () => this.loadCodeAndFocus());

    // Make auto-completion show up right after the "." was typed.
    editor.commands.on("afterExec", function(e) {
      if (e.command.name === "insertstring" && e.args === ".") {
        editor.execCommand("startAutocomplete");
      }
    });
    this.editor = editor;
    this._successMessage = randomSuccessMessage();

    this.$container.addEventListener('blur', () => this.blur(), true);
    this.$container.addEventListener('focus', () => this.focus(), true);
  }

  async runTest() {
    let $result = this._selectInShadowRoot('.result');
    const allLines = this.editor.session.doc.getAllLines();
    const sourceCode = allLines.join('\n');
    const result = await runTest(sourceCode, this.#runnerOptions);
    if (result.success) {
      const evt = new CustomEvent('change', {bubbles: true, detail: {testPassed: true, message: result?.message}});
      this.dispatchEvent(evt);

      $result.classList.remove('errors');
      $result.classList.add('success');
      $result.innerHTML = this._successMessage;
      this.$container.classList.remove('error');
      this.$container.classList.add('success');
    } else {
      const evt = new CustomEvent('change', {bubbles: true, detail: {testPassed: false, message: result?.message}});
      this.dispatchEvent(evt);

      $result.classList.add('errors');
      $result.classList.remove('success');
      $result.innerHTML = `<span class="message">${result.message}</span><a class="reset">Reset</a>`;
      $result.querySelector('.reset').addEventListener('click', () => this.onReset());
      // removeEventListener is unnecessary when DOM gets overwritten
      this.$container.classList.add('error');
      this.$container.classList.remove('success');
    }
  }

  onReset() {
    this._isLoaded = false;
    this.loadCodeAndFocus();
  }

  disconnectedCallback() {
    this.$container.removeEventListener('blur', () => this.blur(), true);
    this.$container.removeEventListener('focus', () => this.focus(), true);
  }

  loadCode(options = {cleanMetaLine: true}) {
    if (false === this._isLoaded) {
      const code = cleanedSourceCode(this.textContent);
      this.#runnerOptions = {...this.#runnerOptions, ...readRunnerOptions(code)};
      this.editor.setValue(options.cleanMetaLine ? cleanMetaLine(code) : code);
      this._isLoaded = true;
      this.shadowRoot.querySelector('.result').style.display = 'flex';
    }
    this.editor.clearSelection();
  }

  loadCodeAndFocus(options = {cleanMetaLine: true}) {
    this.loadCode(options);
    this.editor.clearSelection();
    this.editor.selection.moveCursorTo(0, 0);
    this.delayedFocus();
  }

  delayedFocus() {
    this.editor.focus(); // if this works right away, cool, the backup with the delay is done below. 

    // Let the fading in and showing up finish, then focus it.
    setTimeout(() => this.editor.focus(), 1000);
  }

  focus() {
    // Prevent multiple focus handlings, when not needed.
    if (this.isFocused) {
      return;
    }
    this.isFocused = true;

    this.editor.focus();
    this.$container.classList.add('focus');
  }

  blur() {
    this.editor.textInput.getElement().blur();
    this.$container.classList.remove('focus');
    this.isFocused = false;
  }
}

const cleanedSourceCode = (textContent) => {
  const lines = textContent.split('\n');
  while (lines.length && lines[0].trim() === '') lines.shift(); // remove empty lines at the beginning of the code
  if (lines.length === 0) {
    return '';
  }

  const startIndent = lines[0].match(/^\s*/)[0];
  return lines.map(s => s.substring(startIndent.length)).join('\n');
};

const cleanMetaLine = (code) => {
  const lines = code.split('\n');
  const isLineWithMetaContent = lines[0].trimStart().startsWith('//:');
  if (lines.length > 0 && isLineWithMetaContent) {
    lines.shift();
  }
  return lines.join('\n');
}

const updateEditorHeight = (editor) => {
  const lineCount = editor.session.getLength();
  const lineHeight = 1.1;
  const preventEditorBoxFromJumping = 0.1;
  editor.container.style.height = `${lineCount * lineHeight + preventEditorBoxFromJumping}rem`;
  editor.resize();
}

/**
 * The following are the types used in this file.
 *
 * @typedef {string} SourceCode
 * @typedef {{strictMode: boolean} | {topLevelAwait: boolean} | {runGivenCodeOnly: boolean, type?: 'module'}} RunnerOptions
 * @typedef {{success: boolean, message: string, code?: unknown}} TestRunResult
 */

/**
 * @param sourceCode {SourceCode}
 * @returns {RunnerOptions | {}}
 */
const readRunnerOptions = (sourceCode) => {
  const firstLine = sourceCode.trim().split('\n')[0].trim();
  if (firstLine.startsWith('//: {"jskatas"')) {
    const optionsString = firstLine.substring(3).trim();
    return JSON.parse(optionsString)?.jskatas.runnerOptions;
  }
  return {};
};

const AsyncFunction = async function() {}.constructor;

/**
 * Check for a SyntaxError before running the code in the worker,
 * since a worker running with type=module is very sparse with error messages.
 * For a syntax error it only reports "error" and I see not what's the cause, this
 * is not helpful for the user.
 * 
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions}
 * @returns {Error|false}
 */
const checkForSyntaxError = (sourceCode, options) => {
  if (options.runGivenCodeOnly && options.type === 'module') {
    // We can not put the sourceCode inside a function if there is for example an `import` in it, so just skip this test. 
    return false;
  }
  try {
    new AsyncFunction(sourceCode);
  } catch (e) {
    return e;
  }
  return false;
};

/**
 * This is the main entry point for the test runner.
 *
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions}
 * @returns {Promise<TestRunResult>}
 */
const runTest = async (sourceCode, options) => {
  const possiblySyntaxError = checkForSyntaxError(sourceCode, options);
  if (possiblySyntaxError instanceof Error) {
    return Promise.resolve({success: false, message: String(possiblySyntaxError)});
  }
  if (sourceCode.trim() === '') {
    return Promise.resolve({success: false, message: 'This looks like there is nothing tested.'});
  }
  return await runCodeInWorker(sourceCode, options);
}

/**
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions}
 * @returns {Blob}
 */
const buildWorkerBlob = (sourceCode, options) => {
  if (options.runGivenCodeOnly) {
    return new Blob([
      `
      ${sourceCode}
      ;self.postMessage('ok');`
    ], {type: 'text/javascript'});
  }

  const origin = location.origin;
  const lines = options.strictMode ? [
    '"use strict";',
    'globalThis.parcelRequire = undefined;', // Needed by the assert.js below, since its parcel bundled :shrug:.
    `const assertImport = import("${origin}/assert.js");`,
  ] : [
    `importScripts("${origin}/assert.js");`,
    'const assertImport = Promise.resolve();',
  ];
  const blob = new Blob([
    lines.join('\n') + `
      const test = async () => {
        ${sourceCode}
      };
      
      // Define the 'self.onmessage' as an async function, so we can use await inside, some tests use promises
      // run asynchronously.
      self.onmessage = (async () => {
        await assertImport; // Wait for the assert.js to be loaded just here to make sure that 'self.onmessage' is run and the script can be started from the outside.
        try {
          const maybePromise = test(); 
          
          // Wait for 100ms, so a promise that might be slow has the chance to fulfill.
          // If our \`maybePromise\` is faster it will just make this block terminate asap.
          const waitingPromise = new Promise(resolve => setTimeout(resolve, 100));
          await Promise.race([maybePromise, waitingPromise]);
    
          // Check if the actual result is still pending, if so we report it back as pending.
          const isPendingPromise = await Promise.race([maybePromise, 'done']) === 'done';
          if (isPendingPromise) {
            self.postMessage(JSON.stringify({success: false, message: 'Promise is pending'}));
          } else {
            self.postMessage('ok');
          }
        } catch(e) {
          const keys = Object.getOwnPropertyNames(e);
          const msg = JSON.stringify(keys.reduce((prev, cur) => {prev[cur] = e[cur]; return prev}, {}))
          self.postMessage(msg); 
        }
      });
    `
  ], {type: 'text/javascript'});
  return blob;
};

/**
 * @param options {RunnerOptions}
 * @returns {boolean}
 */
const isTypeModule = (options) => {
  if (options.strictMode === false) {
    return false;
  }
  if (options.runGivenCodeOnly) {
    if (options.type === 'module') {
      return true;
    }
    return false;
  }
  return true;
}

/**
 * This runs the given source code in a worker thread, configuring it as given by the options.
 *
 * @param sourceCode {SourceCode}
 * @param options {RunnerOptions}
 * @returns {Promise<TestRunResult>}
 */
const runCodeInWorker = async (sourceCode, options) => {
  let theResolve;
  const returnPromise = new Promise((resolve) => {
    theResolve = resolve;
  });
  const blob = buildWorkerBlob(sourceCode, options);

  const closeWorker = (worker) => {
    worker.terminate();
  };

  const workerOptions = {
    ...(isTypeModule(options) ? {type: 'module'} : {})
  };
  const worker = new Worker(URL.createObjectURL(blob), workerOptions);

  worker.onerror = (event) => {
    theResolve({success: false, message: event.message ?? String(event)});
    closeWorker(worker);
  };
  worker.onmessage = (event) => {
    if (event.data === 'ok') {
      theResolve({success: true, message: 'Exited', code: event.data.code});
    } else {
      const data = JSON.parse(event.data);
      theResolve({success: false, message: data.message, code: data.code});
    }
    closeWorker(worker);
  };
  worker.onmessageerror = (event) => {
    theResolve({success: false, message: 'messageerror:' + String(event)});
  };
  // Run the worker code
  worker.postMessage('');
  return returnPromise;
}

const successMessages = [
  "🌟 Brilliant!",
  "🎈 Mastery in every glimpse.",
  "💡 A fresh spark or a known glow, well done!",
  "🌱 Growth is constant.",
  "📖 Every chapter counts.",
  "🔥 New flames or old fires.",
  "🌉 Every step strengthens your knowledge.",
  "🍀 A new find or treasured gem, cherish it!",
  "🏆 Fresh conquest or celebrated return, win!",
  "🍪 Savor learning.",
  "🎊 Celebrate each bit.",
  "🎓 New discovery or cherished memory, kudos!",
  "🚀 Maiden voyage or return trip, soar high!",
  "💪 New hurdle or familiar obstacle, you've got this!",
  "🎁 Every gift is special.",
];
const randomSuccessMessage = () => successMessages[Math.floor(Math.random() * successMessages.length)];

customElements.define('jskatas-test-editor', TestEditor);
