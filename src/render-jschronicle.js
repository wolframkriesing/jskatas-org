import {renderTemplate} from './_deps/render-template.js';
import {chronicle, facts, meta} from "./jschronicle.js";
import {writeOutputFile} from "./_deps/fs.js";

const addPercent = (data, minPercentage = 0) => {
  const values = Object.values(data);
  const maxValue = Math.max(...values.map(({numTotal}) => numTotal));
  return Object.fromEntries(Object.keys(data).map(key => {
    const normalizedValue =
      maxValue === 0 
        ? 0 
        : (data[key].numTotal / maxValue) * (1 - minPercentage) + minPercentage;
    return [key, {...data[key], percent: normalizedValue}];
  }));
};

const sumOfMethodsAndProps = obj => Object.entries(obj).reduce((acc, [_, value]) => acc + (value?.length ?? 0), 0);
const sumOfAll = (filterFor = '') => (all, [esVersion, object]) => {
  let filteredEntries;
  if (filterFor === '') {
    filteredEntries = Object.entries(object);
  } else {
    filteredEntries = filterFor in object ? [[filterFor, object[filterFor]]] : []
  }
  all[esVersion] = {
    numTotal: filteredEntries.reduce((acc, [_, value]) => acc + sumOfMethodsAndProps(value), 0),
    numMethods: filteredEntries.reduce((acc, [_, value]) => acc + (value?.methods?.length ?? 0), 0),
    numStaticMethods: filteredEntries.reduce((acc, [_, value]) => acc + (value?.staticMethods?.length ?? 0), 0),
    numProperties: filteredEntries.reduce((acc, [_, value]) => acc + (value?.properties?.length ?? 0), 0),
  };
  return all;
}

export const generateJsChroniclePages = async ({pageParams}) => {
  const keys = new Set([
    ...Object.entries(chronicle).map(([key, _]) => Object.keys(chronicle[key])).flat(),
  ]);
  const data = {
    all: addPercent(Object.entries(chronicle).reduce(sumOfAll(), {})),
    ...Object.fromEntries([...keys]
      .map(key => [key, addPercent(Object.entries(chronicle).reduce(sumOfAll(key), {}))])
    ),
  };
  const stats = {data, allYears: Object.keys(data.all), facts};

  {
    const template = 'jschronicle/all.njk.html';
    const outFile = 'jschronicle/all.html';
    const renderedFile = await renderTemplate(template, {
      ...pageParams,
      meta,
      pageTitlePrefix: 'JSChronicle - All',
      stats
    });
    await writeOutputFile(outFile, renderedFile);
  }

  {
    let allBuiltins = new Set();
    const builtinsByVersion = new Map();
    Object.entries(chronicle).forEach(([key, value]) => {
      const lastSize = allBuiltins.size;
      allBuiltins = new Set([...allBuiltins, ...Object.keys(value)]);
      builtinsByVersion.set(key, {all: allBuiltins.size, existed: lastSize, added: allBuiltins.size - lastSize, addedNames: [...allBuiltins].slice(lastSize)});
    });
    const maxSize = Math.max(...[...builtinsByVersion.values()].map(({all}) => all));
    builtinsByVersion.forEach((value, key) => {
      builtinsByVersion.set(key, {
        ...value, 
        percent: { 
          all: Math.round(value.all / maxSize * 100), 
          existed: Math.round(value.existed / maxSize * 100), 
          added: Math.round(value.added / maxSize * 100) 
        }
      });
    });
    
    // extract all builtins as key and all properties as values in a set
    const all = {};
    Object.entries(chronicle).forEach(([esVersion, objects]) => {
      all[esVersion] = {};
      Object.entries(objects).forEach(([obj, propTypes]) => {
        Object.entries(propTypes).forEach(([propType, propNames]) => {
          if (propNames.length === 0) {
            return;
          }
          if (false === (obj in all[esVersion])) {
            all[esVersion][obj] = new Set();
          }
          all[esVersion][obj] = new Set([...all[esVersion][obj], ...propNames]);
        });
      });
    });
    
    const thisYear = (new Date).getFullYear();
    const metaByYear = Object.fromEntries(Object.entries(meta).map(([key, value]) => [value.publishedAt[1], value]));
    const jsWasBornYear = 1995;
    const yearsSinceJsWasBorn = Array
      .from({length: thisYear - jsWasBornYear + 1}, (_, i) => i + jsWasBornYear)
      // .filter(year => false === (year > 2002 && year < 2009))
      // .map(year => year === 2002 ? '...' : year)
    ;
    const template = 'jschronicle/index.njk.html';
    const outFile = 'jschronicle/index.html';
    const renderedFile = await renderTemplate(template, {
      ...pageParams, meta, yearsSinceJsWasBorn, metaByYear, builtinsByVersion, numBuiltins: allBuiltins.size, all,
      pageTitlePrefix: 'JSChronicle',
      stats
    });
    await writeOutputFile(outFile, renderedFile);
  }
}
